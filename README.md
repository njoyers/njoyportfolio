<a name="readme-top"></a>

<br />
<div align="center">
  <h1 align="center">NJoyPortfolio</h1>
  <h3 align="center">The frontend part of my personnal project</h3>
</div>

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li>
        <a href="#usage">Usage</a>
        <ul>
            <li><a href="#development-server">Development Server</a></li>
            <li><a href="#documentation">Documentation</a></li>
        </ul>
    </li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## About the project

This project is a portfolio of my realizations from both work and personal experiences. The web site contains several pages: home page, blogs/articles, cv page, contact page.
To design and develop the user interface, this project uses Angular.

<!-- GETTING STARTED -->

## Getting started

### Prerequisites

Install [Node.js](https://nodejs.org/fr) which includes [Node Package Manager](https://docs.npmjs.com/getting-started)

### Installation

You must install the project dependencies with the following command:

```sh
npm install -g @angular/cli
npm install
```

Then you have to store API keys and other sensitive data in the OS environments variables manager.

<!-- USAGE -->

## Usage

### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

### Documentation

You can check the [Angular](https://angular.dev/overview) documentation for more information.

<!-- CONTACT -->

## Contact

Benjamin Gédéon - benjamingedeon1996@gmail.com

Project Link: [https://gitlab.com/njoyers/njoyportfolio](https://gitlab.com/njoyers/njoyportfolio)

<p align="right">(<a href="#readme-top">back to top</a>)</p>
