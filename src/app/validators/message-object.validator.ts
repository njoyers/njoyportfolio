import { AbstractControl } from '@angular/forms';

export function messageObjectValidator(
  control: AbstractControl
): Record<string, boolean> | null {
  const messageObject = control;
  if (typeof messageObject !== 'object' || messageObject === null) {
    return { invalidMessageObject: true };
  }
  if (messageObject?.value?.key === 'default') {
    return { required: true };
  }
  return null;
}
