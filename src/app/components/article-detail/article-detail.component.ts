import {
  AfterViewInit,
  Component,
  ElementRef,
  inject,
  Renderer2,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ArticleService } from '@services/articles/article.service';
import { Article, isArticle } from '@models/article.model';

@Component({
  selector: 'article-detail',
  standalone: true,
  imports: [],
  templateUrl: './article-detail.component.html',
  styleUrls: ['./article-detail.component.scss'],
})
export class ArticleDetailComponent implements AfterViewInit {
  private articleService = inject(ArticleService);
  private route = inject(ActivatedRoute);
  private renderer = inject(Renderer2);

  @ViewChild('articleContent') articleContent:
    | ElementRef<HTMLDivElement>
    | undefined;

  id = 0;
  title = '';
  content = '';
  article: Article = {
    id: 0,
    title: '',
    html_content: '',
    custom_css: '',
    created_at: '',
    updated_at: '',
    is_archived: false,
  };

  ngAfterViewInit() {
    this.route.params.subscribe(params => {
      // console.log("params", params);
      this.id = parseInt(params['id']);
    });
    this.articleService.getArticleById(this.id).subscribe({
      next: res => {
        if (!res) {
          console.log('No article found');
          return;
        }
        if (!isArticle(res)) throw Error('Wrong article type');
        if (!this.articleService.articleList.find(a => a.id === res.id)) {
          this.articleService.articleList.push(res);
        }
        this.article = res;
      },
      error: error => console.error(error),
      complete: () => {
        console.info('Article loaded');
        if (!this.articleContent) return;

        this.renderer.setProperty(
          this.articleContent.nativeElement,
          'innerHTML',
          this.article.html_content
        );
        this.renderer.setStyle(
          this.articleContent.nativeElement,
          'cssText',
          this.article.custom_css
        );
      },
    });
  }
}
