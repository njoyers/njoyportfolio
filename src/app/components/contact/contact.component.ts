import { CommonModule } from '@angular/common';
import { Component, OnInit, inject } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { messageObjectValidator } from '@validators/message-object.validator';
import { LoggingService } from '@services/logging/logging.service';
import { ContactService } from '@services/contact/contact.service';
import { debounceTime, distinctUntilChanged } from 'rxjs';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { Router } from '@angular/router';
import { TranslationPipe } from '@pipes/i18n/translation.pipe';
import { TranslationService } from '@services/i18n/translation.service';

@Component({
  selector: 'contact',
  standalone: true,
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    TranslationPipe,
  ],
  templateUrl: './contact.component.html',
  styleUrl: './contact.component.scss',
})
export class ContactComponent implements OnInit {
  private contactService = inject(ContactService);
  private formBuilder = inject(FormBuilder);
  private loggingService = inject(LoggingService);
  private router = inject(Router);
  private translationService = inject(TranslationService);

  contactForm!: FormGroup;
  triedToSubmit = false; // for onSubmit

  objectOptions = [
    { key: 'default', label: '' },
    { key: 'contact', label: 'KEY_SUBJECT_CONTACT_CHOICE' },
    { key: 'help', label: 'KEY_SUBJECT_HELP_CHOICE' },
    { key: 'recrut', label: 'KEY_SUBJECT_RECRUIT_CHOICE' },
    { key: 'other', label: 'KEY_SUBJECT_OTHER_CHOICE' },
  ];
  initialOption =
    this.objectOptions.find(opt => opt.key === 'default') ||
    this.objectOptions[0];
  selectedOption = this.initialOption;

  ngOnInit(): void {
    this.contactForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      messageObject: [
        this.selectedOption,
        [Validators.required, messageObjectValidator],
      ],
      message: ['', [Validators.required, Validators.maxLength(2000)]],
    });
  }

  get firstName() {
    return this.contactForm.get('firstName');
  }

  get lastName() {
    return this.contactForm.get('lastName');
  }

  get email() {
    return this.contactForm.get('email');
  }

  get messageObject() {
    return this.contactForm.get('messageObject');
  }

  get message() {
    return this.contactForm.get('message');
  }

  onReset(): void {
    this.contactForm?.reset();
    this.selectedOption = this.initialOption;
    this.contactForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      messageObject: [
        this.selectedOption,
        [Validators.required, messageObjectValidator],
      ],
      message: ['', [Validators.required, Validators.maxLength(2000)]],
    });
  }

  onSubmit(): void {
    this.triedToSubmit = true;

    if (this.contactForm?.valid) {
      this.contactService
        .postForm(this.contactForm)
        .pipe(debounceTime(500), distinctUntilChanged())
        .subscribe({
          next: data => {
            this.contactForm?.reset();
            this.loggingService.info(JSON.stringify(data));
            // console.log(data);
          },
          error: error => {
            this.loggingService.error(error);
            // console.log(error);
          },
          complete: () => {
            this.loggingService.info('Form submitted');
            // console.log("complete");
            this.router.navigate(['form-submitted']);
          },
        });
      return;
    }

    // check form validity
    for (const fieldName in this.contactForm?.controls) {
      const field = this.contactForm?.controls[fieldName];

      if (field?.invalid) {
        // show error message
        this.loggingService.error(`Error in field: ${fieldName}`);
        // console.error("Errors", fieldName, field)
      }
    }
  }

  autoGrow(event: Event) {
    const textarea = event.target as HTMLTextAreaElement;
    textarea.style.height = 'auto';
    textarea.style.height = `${textarea.scrollHeight}px`;
  }
}
