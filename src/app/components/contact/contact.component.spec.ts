import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { ContactComponent } from './contact.component';
import { ContactService } from '@services/contact/contact.service';
import { LoggingService } from '@services/logging/logging.service';

describe('ContactComponent', () => {
  let component: ContactComponent;
  let fixture: ComponentFixture<ContactComponent>;

  const ContactServiceStub: Partial<ContactService> = {
    postForm: () => {
      return of({ message: 'Email sent successfully.' });
    },
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ContactComponent],
      providers: [
        { provide: ContactService, useValue: ContactServiceStub },
        FormBuilder,
        LoggingService,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(ContactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', (done: DoneFn) => {
    expect(component).toBeTruthy();
    done();
  });

  it('should have a contact form', (done: DoneFn) => {
    expect(component.contactForm).toBeTruthy();
    expect(component.contactForm.valid).toBeFalsy();

    component.contactForm.controls['firstName'].setValue('testfirst');
    component.contactForm.controls['lastName'].setValue('testlast');
    component.contactForm.controls['email'].setValue('testem@il');
    component.contactForm.controls['messageObject'].setValue('testobject');
    component.contactForm.controls['message'].setValue('testmessage');
    component.contactForm.markAllAsTouched();
    expect(component.contactForm.valid).toBeTruthy();

    done();
  });

  it('should reset the form', (done: DoneFn) => {
    expect(component.contactForm).toBeTruthy();
    expect(component.contactForm.valid).toBeFalsy();

    component.contactForm.controls['firstName'].setValue('testfirst');
    component.contactForm.controls['lastName'].setValue('testlast');
    component.contactForm.controls['email'].setValue('testem@il');
    component.contactForm.controls['messageObject'].setValue('testobject');
    component.contactForm.controls['message'].setValue('testmessage');
    component.contactForm.markAllAsTouched();
    expect(component.contactForm.valid).toBeTruthy();

    component.onReset();
    expect(component.contactForm.valid).toBeFalsy();
    expect(component.contactForm.controls['firstName'].value).toBe('');
    expect(component.contactForm.controls['lastName'].value).toBe('');
    expect(component.contactForm.controls['email'].value).toBe('');
    expect(component.contactForm.controls['messageObject'].value).toBe(
      component.initialOption
    );
    expect(component.contactForm.controls['message'].value).toBe('');
    done();
  });

  it('should send an email', (done: DoneFn) => {
    expect(component.contactForm).toBeTruthy();
    component.onSubmit();

    expect(component.contactForm.valid).toBeFalsy();

    component.contactForm.controls['firstName'].setValue('testfirst');
    component.contactForm.controls['lastName'].setValue('testlast');
    component.contactForm.controls['email'].setValue('testem@il');
    component.contactForm.controls['messageObject'].setValue('testobject');
    component.contactForm.controls['message'].setValue('testmessage');
    component.contactForm.markAllAsTouched();
    expect(component.contactForm.valid).toBeTruthy();

    component.onSubmit();
    done();
  });
});
