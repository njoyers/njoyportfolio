import { Component } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule } from '@angular/router';
import { TranslationPipe } from '@pipes/i18n/translation.pipe';

@Component({
  selector: 'form-submitted-page',
  templateUrl: './form-submitted-page.component.html',
  styleUrls: ['./form-submitted-page.component.scss'],
  standalone: true,
  imports: [MatButtonModule, RouterModule, TranslationPipe],
})
export class FormSubmittedPageComponent {}
