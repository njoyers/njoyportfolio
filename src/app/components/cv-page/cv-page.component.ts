import { Component } from '@angular/core';
import { ICONS_PATH } from '@assets/icons';
import { TranslationPipe } from '@pipes/i18n/translation.pipe';

@Component({
  selector: 'cv-page',
  standalone: true,
  imports: [TranslationPipe],
  templateUrl: './cv-page.component.html',
  styleUrl: './cv-page.component.scss',
})
export class CvPageComponent {
  iconsPath = ICONS_PATH;
}
