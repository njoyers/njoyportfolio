import {
  ChangeDetectorRef,
  Component,
  ErrorHandler,
  inject,
} from '@angular/core';
import { MatSelectModule } from '@angular/material/select';
import { LANGS } from '@models/lang.model';
import { TranslationPipe } from '@pipes/i18n/translation.pipe';
import { TranslationService } from '@services/i18n/translation.service';

@Component({
  selector: 'language-button',
  standalone: true,
  imports: [MatSelectModule, TranslationPipe],
  templateUrl: './language-button.component.html',
  styleUrl: './language-button.component.scss',
})
export class LanguageButtonComponent {
  private translationService = inject(TranslationService);
  private changeDetectorRef = inject(ChangeDetectorRef);
  private errorhandler = inject(ErrorHandler);
  selectedLanguage = this.translationService.defaultLangCode;
  readonly languages = LANGS;

  onLanguageChange() {
    this.translationService.setLanguage(this.selectedLanguage).subscribe({
      next: () => {
        this.changeDetectorRef.markForCheck();
      },
      error: (err: Error) => {
        this.errorhandler.handleError(err);
      },
      complete: () => {
        console.log('language set');
      },
    });
  }
}
