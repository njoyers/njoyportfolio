import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TranslationPipe } from '@pipes/i18n/translation.pipe';
import { LanguageButtonComponent } from '@components/language-button/language-button.component';
import { ICONS_PATH } from '@assets/icons';

@Component({
  selector: 'footer',
  standalone: true,
  imports: [
    FontAwesomeModule,
    MatFormFieldModule,
    MatSelectModule,
    TranslationPipe,
    LanguageButtonComponent,
  ],
  templateUrl: './footer.component.html',
  styleUrl: './footer.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FooterComponent implements OnInit {
  linkedinHover = false;
  gitlabHover = false;
  njoyBlogHover = false;
  icons = ICONS_PATH;

  readonly linkedinIconPath = this.icons.LINKEDIN;
  readonly linkedinAltIconPath = this.icons.LINKEDINALT;
  readonly gitlabIconPath = this.icons.GITLAB;
  readonly gitlabAltIconPath = this.icons.GITLABALT;
  readonly njoyBlogIconPath = this.icons.NJOYBLOG;

  readonly linkedinLink = 'https://www.linkedin.com/in/benjamin-gedeon/';
  readonly gitlabLink = 'https://gitlab.com/njoyers';
  readonly njoyBlogLink = 'https://njoyers.fr';

  readonly iconsWidth = '32px';
  readonly iconsHeight = '32px';

  ngOnInit(): void {
    console.log('footer loaded');
  }
  linkedinIconMouseOver() {
    this.linkedinHover = true;
  }

  linkedinIconMouseLeave() {
    this.linkedinHover = false;
  }

  mouseOnGitlabIcon(bool: boolean) {
    this.gitlabHover = bool;
  }
  mouseOnNjoyBlogIcon(bool: boolean) {
    this.njoyBlogHover = bool;
  }
}
