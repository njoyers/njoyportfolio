import { NgStyle } from '@angular/common';
import { ChangeDetectorRef, Component, inject, OnInit } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { RouterOutlet, RouterLink } from '@angular/router';
import { ArticleList, isArticleList } from '@models/article.model';
import { ArticleService } from '@services/articles/article.service';
import { TranslationPipe } from '@pipes/i18n/translation.pipe';

@Component({
  selector: 'home-page',
  standalone: true,
  imports: [
    NgStyle,
    RouterOutlet,
    RouterLink,
    MatCardModule,
    MatButtonModule,
    TranslationPipe,
  ],
  templateUrl: './home-page.component.html',
  styleUrl: './home-page.component.scss',
})
export class HomePageComponent implements OnInit {
  private articleService = inject(ArticleService);

  firstHomeImgpath = 'assets/images/fotor-ai-dev_dans_bureau.jpg';
  articlesManager: ArticleList = [];
  isLoadingArticles = true;

  constructor(private cd: ChangeDetectorRef) {}

  ngOnInit(): void {
    this.articleService.getArticles().subscribe({
      next: res => {
        if (!res) {
          console.log('No articles found');
          return;
        }
        if (!isArticleList(res)) {
          console.log('Wrong article type');
          return;
        }
        for (const article of res) {
          if (!this.articleService.articleList.find(a => a.id === article.id)) {
            this.articleService.articleList.push(article);
          }
        }
        this.articlesManager = this.articleService.articleList;
      },
      error: error => console.error(error),
      complete: () => {
        console.info('All articles loaded');
        this.cd.markForCheck();
        this.isLoadingArticles = false;
      },
    });
  }
}
