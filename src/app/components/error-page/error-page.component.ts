import { Component } from '@angular/core';
import { TranslationPipe } from '@pipes/i18n/translation.pipe';

@Component({
  selector: 'error-page',
  standalone: true,
  imports: [TranslationPipe],
  templateUrl: './error-page.component.html',
  styleUrl: './error-page.component.scss',
})
export class ErrorPageComponent {}
