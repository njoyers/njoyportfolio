import { Component, output, OutputEmitterRef } from '@angular/core';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { TranslationPipe } from '@pipes/i18n/translation.pipe';

@Component({
  selector: 'nav-bar',
  standalone: true,
  imports: [
    MatIconModule,
    MatToolbarModule,
    RouterModule,
    MatSidenavModule,
    MatButtonModule,
    TranslationPipe,
  ],
  templateUrl: './nav-bar.component.html',
  styleUrl: './nav-bar.component.scss',
})
export class NavBarComponent {
  openMenu: OutputEmitterRef<void> = output();

  handleMenu() {
    this.openMenu.emit();
  }
}
