import { Component } from '@angular/core';
import { RouterOutlet, RouterLink, RouterLinkActive } from '@angular/router';
import { NavBarComponent } from '@components/nav-bar/nav-bar.component';
import { ErrorPageComponent } from '@components/error-page/error-page.component';
import { FooterComponent } from '@components/footer/footer.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { TranslationPipe } from '@pipes/i18n/translation.pipe';
import { LanguageButtonComponent } from '@components/language-button/language-button.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [
    RouterOutlet,
    RouterLink,
    RouterLinkActive,
    NavBarComponent,
    ErrorPageComponent,
    FooterComponent,
    MatSidenavModule,
    MatIconModule,
    MatButtonModule,
    TranslationPipe,
    LanguageButtonComponent,
  ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent {
  isMenuOpened = false;

  toggleMenu() {
    this.isMenuOpened = !this.isMenuOpened;
  } // ou utiliser dans le html, sidenav.toggle() => this.isMenuOpened = !this.isMenuOpened

  closeMenu() {
    this.isMenuOpened = false;
  } // ou utiliser dans le html, sidenav.close()  => this.isMenuOpened = false

  openMenu() {
    this.isMenuOpened = true;
  } // ou utiliser dans le html, sidenav.open()  => this.isMenuOpened = true
}
