import {
  Component,
  inject,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { RouterLink, RouterLinkActive } from '@angular/router';
import { ArticleService } from '@services/articles/article.service';
import { ArticleDetailComponent } from '@components/article-detail/article-detail.component';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { Article, isArticleList } from '@models/article.model';
import { TranslationPipe } from '@pipes/i18n/translation.pipe';

@Component({
  selector: 'article-list',
  standalone: true,
  imports: [
    RouterLink,
    RouterLinkActive,
    ArticleDetailComponent,
    MatButtonModule,
    MatCardModule,
    TranslationPipe,
  ],
  templateUrl: './article-list.component.html',
  styleUrl: './article-list.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ArticleListComponent implements OnInit {
  articleService = inject(ArticleService);
  articlesManager: Article[] = [];
  isLoadingArticles = true;

  constructor(private cd: ChangeDetectorRef) {}

  ngOnInit(): void {
    this.articleService.getArticles().subscribe({
      next: res => {
        if (!res) {
          console.log('No articles found');
          return;
        }
        if (!isArticleList(res)) {
          console.log('Wrong article type');
          return;
        }
        for (const article of res) {
          if (!this.articleService.articleList.find(a => a.id === article.id)) {
            this.articleService.articleList.push(article);
          }
        }
        this.articlesManager = this.articleService.articleList;
      },
      error: error => console.error(error),
      complete: () => {
        console.info('All articles loaded');
        this.cd.markForCheck();
        this.isLoadingArticles = false;
      },
    });
  }
}
