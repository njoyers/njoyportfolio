import { inject, Pipe, PipeTransform } from '@angular/core';
import { TranslationService } from '@services/i18n/translation.service';

@Pipe({
  name: 'translate',
  pure: false,
  standalone: true,
})
export class TranslationPipe implements PipeTransform {
  private translationService = inject(TranslationService);

  transform(key: string): string {
    return this.translationService.translate(key);
  }
}
