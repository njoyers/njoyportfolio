import { APP_INITIALIZER, ApplicationConfig } from '@angular/core';
import { provideRouter, withPreloading } from '@angular/router';
import { provideHttpClient } from '@angular/common/http';

import { routes } from './app.routes';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import { CustomPreloader } from './routing-loader/custom-preloader';
import { TranslationService } from '@services/i18n/translation.service';

function initializeAppFactory(translationService: TranslationService) {
  return () => translationService.loadTranslations();
}

export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(routes, withPreloading(CustomPreloader)),
    provideHttpClient(),
    provideAnimationsAsync(),
    CustomPreloader,
    {
      provide: APP_INITIALIZER,
      useFactory: initializeAppFactory,
      multi: true,
      deps: [TranslationService],
    },
  ],
};
