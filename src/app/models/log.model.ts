export enum LogLevel {
  Debug = 1,
  Info,
  Warn,
  Error,
  Fatal,
}

export class LogEntry {
  timestamp: Date;
  logLevel: LogLevel;
  message: string;
  constructor(level: LogLevel, message: string) {
    this.timestamp = new Date();
    this.logLevel = level;
    this.message = message;
  }
}
