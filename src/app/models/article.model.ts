export interface Article {
  id: number;
  title: string;
  html_content: string;
  custom_css: string;
  created_at: string;
  updated_at: string;
  is_archived: boolean;
}

export type ArticleList = Article[];

export const isArticle = (article: unknown): article is Article => {
  // added type guard for Article type
  return (
    typeof article === 'object' &&
    article !== null &&
    'id' in article &&
    'title' in article &&
    'html_content' in article &&
    'custom_css' in article &&
    'created_at' in article &&
    'updated_at' in article &&
    'is_archived' in article &&
    typeof article.id === 'number' &&
    typeof article.title === 'string' &&
    typeof article.html_content === 'string' &&
    typeof article.custom_css === 'string' &&
    typeof article.created_at === 'string' &&
    typeof article.updated_at === 'string' &&
    typeof article.is_archived === 'boolean'
  );
};

export const isArticleList = (
  articleList: unknown
): articleList is ArticleList => {
  // added type guard for ArticleList type
  return Array.isArray(articleList) && articleList.every(isArticle);
};
