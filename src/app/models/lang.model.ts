export type LangCode = 'fr-FR' | 'en-US';

export interface Lang {
  code: LangCode;
  name: string;
}

export const LANGS: Lang[] = [
  { code: 'en-US', name: 'English' },
  { code: 'fr-FR', name: 'Français' },
];
