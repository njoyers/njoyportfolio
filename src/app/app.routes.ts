import { Routes } from '@angular/router';
import { HomePageComponent } from '@components/home-page/home-page.component';

export const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'home' },
  { path: 'home', component: HomePageComponent },
  {
    path: 'cv',
    loadComponent: () =>
      import('@components/cv-page/cv-page.component').then(
        m => m.CvPageComponent
      ),
  },
  {
    path: 'contact',
    loadComponent: () =>
      import('@components/contact/contact.component').then(
        m => m.ContactComponent
      ),
  },
  {
    path: 'articles',
    loadComponent: () =>
      import('@components/article-list/article-list.component').then(
        m => m.ArticleListComponent
      ),
    data: { preload: true },
  },
  {
    path: 'articles/:id',
    loadComponent: () =>
      import('@components/article-detail/article-detail.component').then(
        m => m.ArticleDetailComponent
      ),
    data: { preload: true },
  },
  {
    path: 'form-submitted',
    loadComponent: () =>
      import(
        '@components/form-submitted-page/form-submitted-page.component'
      ).then(m => m.FormSubmittedPageComponent),
  },
  {
    path: '**',
    loadComponent: () =>
      import('@components/error-page/error-page.component').then(
        m => m.ErrorPageComponent
      ),
  },
];
