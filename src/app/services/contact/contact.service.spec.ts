import { TestBed, inject } from '@angular/core/testing';
import { ContactService } from './contact.service';
import { provideHttpClient } from '@angular/common/http';
import {
  HttpTestingController,
  provideHttpClientTesting,
} from '@angular/common/http/testing';
import { FormControl, FormGroup } from '@angular/forms';

describe('Service: Contact', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ContactService,
        provideHttpClient(),
        provideHttpClientTesting(),
      ],
    });
  });

  it('should post form', inject(
    [HttpTestingController, ContactService],
    (done: DoneFn) => {
      // Load 'ContactService'
      const service = TestBed.inject(ContactService);
      const testForm: FormGroup = new FormGroup({
        firstName: new FormControl('testfirst'),
        lastName: new FormControl('testlast'),
        email: new FormControl('testemail'),
        messageObject: new FormControl('testobject'),
        message: new FormControl('testmessage'),
      });
      expect(service).toBeTruthy();
      service.postForm(testForm).subscribe(res => {
        expect(res).toEqual({ message: 'Email sent successfully.' });
        done();
      });
    }
  ));
});
