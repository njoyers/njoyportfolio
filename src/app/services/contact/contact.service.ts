import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ContactService {
  private http = inject(HttpClient);

  postForm(form: FormGroup): Observable<unknown> {
    // const url = "http://127.0.0.1:8000" + "/contact-by-mail/";
    const url = 'https://api.njoy-apps.fr' + '/contact-by-mail/';
    const headers: HttpHeaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
    });
    const body = new URLSearchParams();
    // for (const key of Object.keys(form.value)) {
    //     let value = form.value[key];
    //     if (typeof value === 'object') {
    //         value = JSON.stringify(value);
    //     }
    //     body.set(key, value);
    // }
    body.set('first_name', form.value['firstName']);
    body.set('last_name', form.value['lastName']);
    body.set('email', form.value['email']);
    body.set('subject', JSON.stringify(form.value['messageObject']));
    body.set('message', form.value['message']);
    return this.http.post(url, body.toString(), { headers: headers });
  }
}
