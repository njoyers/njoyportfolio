import { HttpClient } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Article, ArticleList } from '@models/article.model';

@Injectable({
  providedIn: 'root',
})
export class ArticleService {
  // private readonly url = "http://127.0.0.1:8000" + "/articles/";
  private readonly url = 'https://api.njoy-apps.fr' + '/articles/';

  http = inject(HttpClient);

  fakeArticles = [
    { id: 1, title: 'Article 1', content: 'Content 1' },
    { id: 2, title: 'Article 2', content: 'Content 2' },
    { id: 3, title: 'Article 3', content: 'Content 3' },
  ];

  articleList: ArticleList = [];

  /**
   * Retrieves the list of articles.
   * @returns An Observable that emits the list of articles.
   */
  getArticles(): Observable<unknown> {
    if (this.articleList.length === 0) {
      return this.http.get(this.url);
    }
    return of(this.articleList);
  }

  getArticleById(id: number) {
    if (
      this.articleList.length === 0 ||
      !this.articleList?.find(a => a.id === id)
    ) {
      return this.http.get<Article>(this.url + id);
    }
    return of(this.articleList.find(a => a.id === id));
  }

  editArticleById(id: number, title: string, content: string) {
    const article = this.fakeArticles.find(a => a.id === id);
    if (!article) throw Error('Article not found');
    article.title = title;
    article.content = content;
    return of(article);
  }

  deleteArticleById(id: number) {
    const article = this.fakeArticles.find(a => a.id === id);
    if (!article) throw Error('Article not found');
    this.fakeArticles = this.fakeArticles.filter(a => a.id !== id);
    return of(article);
  }

  createArticle(title: string, content: string) {
    const id = this.fakeArticles.length + 1;
    const article = { id, title, content };
    this.fakeArticles.push(article);
    return of(article);
  }

  getArticleCount() {
    return of(this.fakeArticles.length);
  }
}
