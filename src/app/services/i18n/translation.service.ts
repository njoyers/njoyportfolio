import { HttpClient } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { LangCode } from '@models/lang.model';
import { map, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class TranslationService {
  protected http = inject(HttpClient);
  protected translations: Record<string, string> = {};
  protected i18nCSVPath = 'assets/i18n/keys.csv';
  defaultLangCode: LangCode = 'fr-FR';

  loadTranslations(lang?: LangCode): Observable<unknown> {
    if (!lang) {
      lang = this.defaultLangCode;
    }
    return this.http.get(this.i18nCSVPath, { responseType: 'text' }).pipe(
      map(data => {
        const allTranslations = this.parseCSV(data);
        this.translations = this.getTranslationsInLang(allTranslations, lang);
      })
    );
  }
  protected getTranslationsInLang(
    allTranslations: Record<string, string>[],
    lang: LangCode
  ): Record<string, string> {
    const dictionary: Record<string, string> = {};
    for (const row of allTranslations) {
      dictionary[row['KEY']] = row[lang];
    }
    return dictionary;
  }

  protected parseCSV(data: string): Record<string, string>[] {
    // to json like object
    const lines = data.split('\n');
    const headers = lines[0].split(';');
    const listOfTranslations: Record<string, string>[] = [];

    for (let i = 1; i < lines.length; i++) {
      const currentLine = lines[i].split(';');
      const rowOfTranslations: Record<string, string> = {};

      for (let j = 0; j < currentLine.length; j++) {
        rowOfTranslations[headers[j].trim()] = currentLine[j].trim();
      }
      listOfTranslations.push(rowOfTranslations);
    }
    return listOfTranslations;
  }

  translate(key: string): string {
    return this.translations[key] || key;
  }

  setDefaultLanguage(lang: LangCode) {
    this.defaultLangCode = lang;
  }

  setLanguage(lang: LangCode): Observable<unknown> {
    return this.loadTranslations(lang);
  }
}
