import { Injectable } from '@angular/core';
import { LogEntry, LogLevel } from '@models/log.model';

@Injectable({
  providedIn: 'root',
})
export class LoggingService {
  private logEntries: LogEntry[] = [];
  private logLevel = LogLevel.Debug;

  debug(message: string) {
    this.addEntriesByLevel(LogLevel.Debug, message);
  }

  info(message: string) {
    this.addEntriesByLevel(LogLevel.Info, message);
  }

  warn(message: string) {
    this.addEntriesByLevel(LogLevel.Warn, message);
  }

  error(message: string) {
    this.addEntriesByLevel(LogLevel.Error, message);
  }

  fatal(message: string) {
    this.addEntriesByLevel(LogLevel.Fatal, message);
  }

  setLogLevel(level: LogLevel) {
    this.logLevel = level;
  }

  get entries(): LogEntry[] {
    return this.logEntries;
  }

  get level(): LogLevel {
    return this.logLevel;
  }

  private addEntriesByLevel(level: LogLevel, message: string) {
    if (this.logLevel <= level) {
      this.logEntries.push(new LogEntry(level, message));
    }
  }
}
