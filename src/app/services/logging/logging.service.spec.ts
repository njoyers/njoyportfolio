import { TestBed, inject } from '@angular/core/testing';
import { LoggingService } from './logging.service';
import { LogLevel } from '@models/log.model';

describe('Service: Articles', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoggingService],
    });
  });

  it('should exist', inject([LoggingService], (service: LoggingService) => {
    expect(service).toBeTruthy();
  }));

  it('should set log level', inject(
    [LoggingService],
    (service: LoggingService) => {
      service.setLogLevel(LogLevel.Debug);
      expect(service.level).toBe(LogLevel.Debug);
    }
  ));

  it('should entries be empty', inject(
    [LoggingService],
    (service: LoggingService) => {
      expect(service.entries.length).toBe(0);
    }
  ));

  it('should entries have a debug message', inject(
    [LoggingService],
    (service: LoggingService) => {
      service.setLogLevel(LogLevel.Debug);
      service.debug('debug');
      expect(service.entries.length).toBe(1);
      expect(service.entries[0].logLevel).toBe(LogLevel.Debug);
      expect(service.entries[0].message).toBe('debug');
    }
  ));

  it('should entries have an info message', inject(
    [LoggingService],
    (service: LoggingService) => {
      service.setLogLevel(LogLevel.Info);
      service.info('info');
      expect(service.entries.length).toBe(1);
      expect(service.entries[0].logLevel).toBeLessThanOrEqual(LogLevel.Info);
      expect(service.entries[0].message).toBe('info');
    }
  ));

  it('should entries have a warn message', inject(
    [LoggingService],
    (service: LoggingService) => {
      service.setLogLevel(LogLevel.Warn);
      service.warn('warn');
      expect(service.entries.length).toBe(1);
      expect(service.entries[0].logLevel).toBeLessThanOrEqual(LogLevel.Warn);
      expect(service.entries[0].message).toBe('warn');
    }
  ));

  it('should entries have an error message', inject(
    [LoggingService],
    (service: LoggingService) => {
      service.setLogLevel(LogLevel.Error);
      service.error('error');
      expect(service.entries.length).toBe(1);
      expect(service.entries[0].logLevel).toBeLessThanOrEqual(LogLevel.Error);
      expect(service.entries[0].message).toBe('error');
    }
  ));

  it('should entries have a fatal message', inject(
    [LoggingService],
    (service: LoggingService) => {
      service.setLogLevel(LogLevel.Fatal);
      service.fatal('fatal');
      expect(service.entries.length).toBe(1);
      expect(service.entries[0].logLevel).toBe(LogLevel.Fatal);
      expect(service.entries[0].message).toBe('fatal');
    }
  ));
});
