### Building and running your application

When you're ready, start your application by running:
`docker compose up --build`.

Your application will be available at http://localhost:3000.

### Deploying your application to the cloud

First, build your image, e.g.: `docker build -t myapp .`.
If your cloud uses a different CPU architecture than your development
machine (e.g., you are on a Mac M1 and your cloud provider is amd64),
you'll want to build the image for that platform, e.g.:
`docker build --platform=linux/amd64 -t myapp .`.

Then, push it to your registry, e.g. `docker push myregistry.com/myapp`.

Consult Docker's [getting started](https://docs.docker.com/go/get-started-sharing/)
docs for more detail on building and pushing.

### References

- [Docker's Node.js guide](https://docs.docker.com/language/nodejs/)

### list of commands

To build an image via dockerfile:
`docker build -t njoyportfolio .`

To test the application, on port 80:
`docker run -d -p 80:80 njoyportfolio`

to remove a container, you should first stop it with:
`docker stop CONTAINER_ID|CONTAINER_NAME`

then use :
`docker rm CONTAINER_ID|CONTAINER_NAME`

Then you can remove the image with:
`docker rmi IMAGE_ID|IMAGE_TAG`

Or you can use docker compose with the following:
`docker compose up -d`

and stop with
`docker compose down`
all container are removed at the end. Add the --rmi flag to remove all images

You can remove the cache with :
`docker system prune --all --force`
