### Stage 1 : build ###
FROM node:alpine as build
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm ci
COPY ./ .
RUN npm run build

### Stage 2 : run###
FROM nginx:alpine as production
COPY ./nginx.conf /etc/nginx/nginx.conf
COPY --from=build /usr/src/app/dist/njoy-portfolio/ /app
USER nginx

EXPOSE 3000

# CMD [ "ng", "serve", "--host", "0.0.0.0" ]
# CMD [ "npm", "run", "startapp" ]
